from itertools import chain


class TailCreator:
    def __init__(self, shift_table, xref_shift):
        """
        shift_table - table, that contains correspondinces of
        pdf_object obj_number to bytes shift in file. obj_number
        starts from 1 and goes to the len(shift_table) including.
        One should pass a correct shift of xref in file.
        """
        self._shift_table = shift_table
        self._xref_shift = xref_shift
        self._size = len(self._shift_table) + 1

    def get_tail(self):
        """
        Return str representation of xref and trailer.
        """
        zero_obj = "0000000000 65535 f"
        shifts = map(self._shift_table.get, range(1, self._size))
        objects = map("{:0>10} 00000 n".format, shifts)

        xref = "xref\n{size} 0\n{table}".format(
            size=self._size,
            table="\n".join(chain((zero_obj,), objects)))

        trailer = "trailer\n<< /Size {size} /Root 1 0 R >>".format(
            size=self._size)
        start_xref = "startxref\n{xref_shift}\n%%EOF".format(
            xref_shift=self._xref_shift)

        return "\n".join((
            xref, trailer, start_xref)).encode("ascii")
