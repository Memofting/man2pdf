from .body.Catalog import Catalog
from .body.Pages import Pages
from .body.Page import Page
from .body.Font import Font
from .body.Contents import Contents
from .body.BodyCreator import BodyCreator
from .tail.TailCreator import TailCreator
import subprocess


class PDFCreator:
    ENCODING = "utf-8"

    def __init__(self, read_fp, write_fp, size):
        """
        It takes file pointer and save it.
        After it reads text from it for encoding to pdf.
        You could tune page size for PDF file, but internal
        constant hasn't arranged for it yet.
        """
        proc = subprocess.Popen(["man", read_fp], stdout=subprocess.PIPE)
        self._read_fp = proc.stdout
        self._write_fp = write_fp
        self.size = size

    def encode_entry(self):
        """
        Main method for encoding txt file to pdf.
        """
        self._write_fp.write(b"%PDF-1.4")
        last_shift = self._write_fp.tell()
        shift_table = {}
        body = BodyCreator(self._read_fp, self.size, PDFCreator.ENCODING)

        pdf_objects = list(body.fp_content2pdf_objects())
        for pdf_object in pdf_objects:
            shift_table[pdf_object.obj_number] = last_shift + 1
            self._write_fp.write(b'\n')
            self._write_fp.write(pdf_object.__str__())
            last_shift = self._write_fp.tell()

        tail = TailCreator(shift_table, self._write_fp.tell() + 1)

        self._write_fp.write(b'\n')
        self._write_fp.write(tail.get_tail())

    def __str__(self):
        pass
