from .Contents import Contents
from .Font import Font
from .StandardObject import StandardObject
# спроси, как избавиться от проблемы, которая возникает, если написать так
# from Pages import Pages - в PAges находится импорт, который заходит в этот
# файл и случается плохое дело.


class Page(StandardObject):
    """
    It collects information needed for creation Page object
    and after setting its parent you could get this informtaion
    in PS like style for pdf encoding.
    """
    def __init__(self, size,
                 contents: Contents, font: Font,
                 ):
        super().__init__()
        self._parent = None
        self._contents = contents
        self._font = font
        assert len(size) == 2
        self._size = size

    def set_parent(self, parent):
        """
        Set parent for this page. Parent is instance of class Pages,
        but due to import loop i cannot write it in code
        """
        assert self._parent is None
        assert parent
        self._parent = parent

    def __str__(self):
        assert self._parent is not None
        dict_obj = "<< /Type /Page\n{par}\n{med}\n{cont}\n{res}\n>>".format(
            par="/Parent {parent_num} 0 R".format(
                parent_num=self._parent.obj_number),
            med="/MediaBox [ 0 0 {w} {h} ]".format(
                w=self._size[0],
                h=self._size[1]
            ),
            cont="/Contents {content_num} 0 R".format(
                content_num=self._contents.obj_number
            ),
            res="/Resources {font_link}".format(
                font_link=self._font.get_link())
        )
        body = "{self.obj_number} 0 obj\n{dict_obj}\nendobj".format(
            self=self, dict_obj=dict_obj)
        return body.encode("ascii")
