from .Catalog import Catalog
from .Pages import Pages
from .Page import Page
from .Font import Font
from .Contents import Contents
from .Encoding import Encoding
from string_alignment.string_aligne import Align


class BodyCreator:
    def __init__(self, read_fp, size, encoding):
        """
        It takes file pointer and save it.
        After it reads text from it for encoding to pdf.
        You could tune page size for PDF file, but internal
        constant hasn't arranged for it yet.
        """
        align = Align(Contents._WIDTH, read_fp)
        self._read_fp = align.get_aligned_text()
        self.size = size
        self.encoding = encoding

    def fp_content2pdf_objects(self):
        """
        With file text create its pdf description and return it.
        """
        pdf_objects = []
        page_objects = []
        encoding = Encoding()
        font = Font("/F1", encoding)
        pdf_objects.append(encoding)
        pdf_objects.append(font)
        for content in self._produce_content(font):
            page = Page(self.size, content, font)
            page_objects.append(page)
            pdf_objects.append(page)
            pdf_objects.append(content)

        pages = Pages(page_objects)
        for page in page_objects:
            page.set_parent(pages)
        catalog = Catalog(pages)

        pdf_objects.append(pages)
        pdf_objects.append(catalog)

        # thus we guarantee that we process object in right order
        pdf_objects.reverse()

        for number, pdf_object in enumerate(pdf_objects, 1):
            pdf_object.set_number(number)
            yield pdf_object

    def _produce_content(self, font):
        """
        Description: It takes lines from read_fp file pointer and gather
                     with them a content in convinient format.
        Modification: read_fp, this
        Result: It shift read_fp from extracted lines and then save that state.
                It also yields new contents object every time, while
                read_fp has had some lines yet.
        """
        # loop for encoding string from file descriptor while they are taking
        while True:
            contents = Contents(font, self.size, self.encoding)
            contents.extract_nessesary_lines(self._read_fp)
            if not contents.strings:
                break
            else:
                yield contents

    def __str__(self):
        pass
