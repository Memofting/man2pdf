from .Pages import Pages
from .StandardObject import StandardObject


class Catalog(StandardObject):
    def __init__(self, pages):
        super().__init__()
        self._pages = pages

    def __str__(self):
        dict_obj = "<< /Type /Catalog\n{pages}\n>>".format(
            pages="/Pages {num} 0 R".format(
                num=self._pages.obj_number))
        body = "{num} 0 obj\n{dict_obj}\nendobj".format(
            num=self.obj_number,
            dict_obj=dict_obj)

        return body.encode("ascii")
