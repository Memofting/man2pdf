import re
from functools import partial
from itertools import islice, chain

from .StandardObject import StandardObject


class Contents(StandardObject):
    """
    Реализует функционал страницы. Помогает с кодированием страницы по
    заданной ширине и высоте.
    """
    _WIDTH = 79
    _HEIGHT = 63

    def __init__(self, font, shift_size, encoding,
                 font_size="12"):
        assert len(shift_size) == 2
        super().__init__()
        self.font_name = font.name
        self.font_size = font_size
        self.encoding = encoding

        self.shift_w = shift_size[0] * 0.01
        self.shift_h = shift_size[1] * 0.99
        self.strings = []

    def extract_nessesary_lines(self, content_iterator):
        """
        Description:
            Choose from content_iterator needed amount of and save them to
            the internal state.
        """
        self.strings = list(islice(content_iterator, Contents._HEIGHT))

    def get_encoded_content(self):
        """
        It encodes strings, that have extracted from fp.
        """
        return b"\n".join(
            map(lambda sb: b"(" + sb + b")'",
                self.to_byte_respresentation(self.strings)))

    def to_byte_respresentation(self, strings):
        """
        This function map each element of strings to it bytes represetation.
        If it needs, than symbol will be replaced with it octal sequence.
        """
        yield from map(partial(re.sub, rb"([\(\)\\])", rb"\\\1"), strings)

    def get_description(self):
        """
        Return description of Content object. It invokes method
        get_encoded_content, so than way of coding is affect at
        first to the get_encoded_content and after on it.
        """
        stream_option = (
            "\nBT\n{self.font_size} TL".format(
                self=self),
            "{self.font_name} {self.font_size} Tf".format(
                self=self),
            "{self.shift_w} {self.shift_h} Td".format(self=self),
            "{size} TL".format(size=self.font_size))

        stream_content = ("\n".join(stream_option).encode("ascii")
                          + self.get_encoded_content()
                          + b"\nET")

        stream = b"stream" + stream_content + b"endstream"
        body = ("{num} 0 obj\n<< /Length {len} >>\n".format(
                    num=self.obj_number,
                    len=len(stream_content)).encode("ascii")
                + stream
                + b"\nendobj")

        return body

    def __str__(self):
        return self.get_description()
