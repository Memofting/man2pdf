from .Page import Page
from .StandardObject import StandardObject


class Pages(StandardObject):
    """
    Pages class contain information needed for pages
    show.
    It wraps its content into pdf dictionary view
    """
    def __init__(self, kids: list):
        """
        Kids: list of page objects that will be shown in document
        """
        super().__init__()
        self._kids = kids

    def __str__(self):
        kids = "/Kids [ {} ]".format(
            " ".join(map("{} 0 R".format,
                         (k.obj_number for k in self._kids))))
        count = "/Count {}".format(len(self._kids))

        dict_obj = "<< /Type /Pages\n{kids}\n{count}\n>>".format(
            kids=kids, count=count)

        body = "{self.obj_number} 0 obj\n{dict_obj}\nendobj".format(
            self=self, dict_obj=dict_obj)

        return body.encode("ascii")
