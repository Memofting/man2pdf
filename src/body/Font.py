from .StandardObject import StandardObject


class Font(StandardObject):
    """
    It shows description about font object.
    """
    def __init__(self, name: str, encoding):
        super().__init__()
        self.name = name
        self._encoding = encoding

    def get_link(self):
        font_info = "<< /Font << {font_name} {font_num} 0 R >> >>".format(
            font_num=self.obj_number,
            font_name=self.name
        )

        return font_info

    def __str__(self):
        body = (
            "{num} 0 obj\n<< /Type /Font\n/Subtype /Type1\n".format(
                num=self.obj_number),
            "/Name {name}\n/BaseFont /Courier\n".format(
                name=self.name),
            "/BaseEncoding /MacRomainEncoding\n",
            "/Encoding {encod} 0 R".format(
                encod=self._encoding.obj_number),
            "\n>>\nendobj")

        return "".join(body).encode("ascii")
