from .StandardObject import StandardObject


class Encoding(StandardObject):
    def __init__(self):
        super().__init__()

    def __str__(self):
        body = (
            "{self.obj_number} 0 obj".format(self=self),
            "<< /Type /Encoding ",
            "/Differences",
            "[",
            "39 /quotesingle",
            "96 /grave",
            "128 /Adieresis /Aring /Ccedilla /Eacute /Ntilde /Odieresis",
            "/aacute /agrave /acircumflex /adieresis /atilde /aring",
            "/eacute /egrave /ecircumflex /edieresis /iacute /igrave",
            "/idieresis /ntilde /oacute /ograve /ocircumflex /odieresis",
            "/uacute /ugrave /ucircumflex /udieresis /dagger /degree /cent",
            "/sterling /section /bullet /paragraph /germandbls /registered",
            "/copyright /trademark /acute /dieresis",
            "174 /AE /Oslash",
            "177 /plusminus",
            "180 /yen /mu",
            "187 /ordfeminine /ordmasculine",
            "190 /ae /oslash /questiondown /exclamdown /logicalnot",
            "196 /florin",
            "199 /guillemotleft /guillemotright /ellipsis",
            "203 /Agrave /Atilde /Otilde /OE /oe /endash /emdash",
            "/quotedblright /quoteleft /quoteright /divide",
            "216 /ydieresis /Ydieresis /fraction /currency /guilsinglleft",
            "/fi /fl /daggerdbl /periodcentered /quotesinglbase /quotedblbase",
            "/perthousand /Acircumflex /Ecircumflex /Aacute /Edieresis",
            "/Iacute /Icircumflex /Idieresis /Igrave /Oacute /Ocircumflex",
            "241 /Ograve /Uacute /Ucircumflex /Ugrave /dotlessi /circumflex",
            "/macron /breve /dotaccent /ring /cedilla /hungarumlaut /ogonek",
            "/caron",
            "]",
            ">>",
            "endobj"
        )

        return "\n".join(body).encode("ascii")
