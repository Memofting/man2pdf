import re
from itertools import chain


class Align:
    """
    Рукописный класс, который позволяет получить текст,
    который в ширину будет меньше, чем указанная длина.

    Для правильной работы требуется адекватное значение длины:
        длина > len(наибольший отступ в тексте)
                + len(слово с наибольшей длиной в тексте)  
    """
    def __init__(self, length, text):
        self.length = length
        self.text = text

    def get_aligned_text(self):
        yield from Align.align_text(self.text, self.length)

    @staticmethod
    def align_text(text, length):
        for paragraph in Align.extract_paragraph(text):
            yield from map(lambda sb: sb + b'\n', Align.full_word_wraparound(paragraph, length))
            yield b'\n'

    @staticmethod
    def extract_paragraph(text):
        lines = []
        for line in text:
            stripped = line.rstrip()
            if stripped:
                lines.append(stripped)
            else:
                yield lines
                lines = []
        if lines:
            yield lines

    @staticmethod
    def full_word_wraparound(text_fragment, length):
        """
        text_fragment - содержит строки, в которых обрезаны завершающие
        символы. 
        Метод обрабатывает строки из итератора с сохранением отступа.
        """
        result_line = b""
        shift = b""
        for line in text_fragment:
            shift_mo = re.match(rb"\s+", line)
            if shift_mo and shift != shift_mo[0]:
                # теперь с начала добиваем новыми пробелами            
                lines = Align.divide_line_to_lines(result_line, length - len(shift))
                yield from map(lambda s: shift + s.lstrip(), lines)

                shift = shift_mo[0]
                result_line = line.lstrip()
                continue

            result_line += line.lstrip()
            # добавить остаток в конце
            if len(result_line) + len(shift) < length:
                continue

            lines = Align.divide_line_to_lines(result_line, length - len(shift))
            yield from map(lambda s: shift + s.lstrip(), lines[:-1])

            result_line = lines[-1].lstrip()

        if result_line:
            lines = Align.divide_line_to_lines(result_line, length - len(shift))
            yield from map(lambda s: shift + s.lstrip(), lines)

    @staticmethod
    def divide_line_to_lines(line, length):
        result = []
        while len(line) > length:
            division_index = line[:length+1].rfind(b' ')

            if division_index <= 0:
                division_index = len(line)

            result.append(line[:division_index])

            line = line[division_index:]

        result.append(line)
        return result
