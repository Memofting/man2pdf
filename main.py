from src.PDFCreator import PDFCreator
import argparse
import os
import sys


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        usage='{} [OPTIONS] MAN_PAGE'.format(os.path.basename(sys.argv[0])),
        description="Create pdf file that contains man page.")
    parser.add_argument('mp', metavar='MAN_PAGE',
                        help='name of man page')

    args = parser.parse_args()
    with open(args.mp + ".pdf", "wb") as write_fp:
        pdf = PDFCreator(args.mp, write_fp, (600, 800))
        pdf.encode_entry()
